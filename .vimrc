" sideshow.bob's .vimrc

" vim mode preferred!
set nocompatible

" enable the syntax highlight mode if available
syntax sync fromstart
if has("syntax")
    syntax sync fromstart
    syntax on
    highlight SpellBad ctermfg=red ctermbg=black term=Underline
endif

" tabs
set softtabstop=4
set shiftwidth=4
set expandtab
set autoindent

" line numbering
set nu

" highlight matches with last search pattern
" set hls

set incsearch           " incremental search
set ignorecase          " ignore the case
set smartcase           " don't ignore the case if the pattern is uppercase
set ruler               " show cursor position
set showmode            " show the current mode
set viminfo=%,'50,\"100,:100,n~/.viminfo        " info to save accross sessions
set backspace=2
normal mz

" when open a new file remember the cursor position of the last editing
if has("autocmd")
    " when editing a file, always jump to the last cursor position
    autocmd BufReadPost * if line("'\"") | exe "'\"" | endif
endif

" encoding
set encoding=utf-8
setglobal fileencoding=utf-8
set nobomb
set fileencodings=ucs-bom,utf-8,iso-8859-1

" colors
set t_Co=256
set background=dark
try
    " load the dracula theme by default
    colorscheme dracula
catch
    " if we cannot load the theme we use our backup colors
    hi User1 ctermfg=green ctermbg=black
    hi User2 ctermfg=yellow ctermbg=black
    hi User3 ctermfg=red ctermbg=black
    hi User4 ctermfg=blue ctermbg=black
    hi User5 ctermfg=white ctermbg=black
endtry

" statusline
set laststatus=2
set statusline=
set statusline +=%1*\ %n\ %*            "buffer number
set statusline +=%5*%{&ff}%*            "file format
set statusline +=%3*%y%*                "file type
set statusline +=%4*\ %<%F%*            "full path
set statusline +=%2*%m%*                "modified flag
set statusline +=%1*%=%5l%*             "current line
set statusline +=%2*/%L%*               "total lines
set statusline +=%1*%4v\ %*             "virtual column number
set statusline +=%2*0x%04B\ %*          "character under cursor

" plugins
:filetype plugin on
" load all pathogen bundles
call pathogen#runtime_append_all_bundles()
call pathogen#helptags()

" bindings
"map <C-d> <ESC>:r! date<CR>kJ$a " put the current date in insert mode
"imap <C-k> <ESC>:r ~/SVC/HEADER<CR>
"imap <C-o> <ESC>:bn<CR>
"imap <C-k> <ESC>:bp<CR>
"imap <C-x> <ESC>:syntax sync fromstart<CR>
"map <C-x> :syntax sync fromstart<CR>
"map <C-o> :bn<CR>
"map <C-k> :bp<CR>
"map 4 $
"vmap q <gv
"vmap <TAB> >gv

" os specific options
if has("win32")
    source $VIMRUNTIME/mswin.vim
    behave mswin
    set guifont=Consolas
endif

