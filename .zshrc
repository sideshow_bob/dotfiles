# emulate 256 colors
export TERM="xterm-256color"

# If you come from bash you might have to change your $PATH.
export PATH=$HOME/bin:$PATH
if [[ -d "$HOME/.local/bin" ]]; then
    export PATH=$HOME/.local/bin:$PATH
fi

# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# theme
# see https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="powerlevel10k/powerlevel10k"

# correction
ENABLE_CORRECTION="true"

# completion
COMPLETION_WAITING_DOTS="true"

# history timestamp
HIST_STAMPS="yyyy-mm-dd"

# plugins
plugins=(
    autoswitch_virtualenv
    autoupdate
    aws
    colored-man-pages
    command-not-found
    common-aliases
    composer
    cp
    docker
    docker-compose
    dotenv
    encode64
    git
    git-extras
    httpie
    jump
    kops
    kubectl
    last-working-dir
    man
    nmap
    npm
    redis-cli
    sudo
    ssh-agent
    svn-fast-info
    systemadmin
    ubuntu
    vscode
    zsh-autosuggestions
)

# ssh-agent
zstyle :omz:plugins:ssh-agent agent-forwarding on
#zstyle :omz:plugins:ssh-agent identities X
zstyle :omz:plugins:ssh-agent lifetime 8h
# oh-my-zsh
source $ZSH/oh-my-zsh.sh

# user configuration

# time
TIMEFMT='%J   %U  user %S system %P cpu %*E total'$'\n'\
'avg shared (code):         %X KB'$'\n'\
'avg unshared (data/stack): %D KB'$'\n'\
'total (sum):               %K KB'$'\n'\
'max memory:                %M MB'$'\n'\
'page faults from disk:     %F'$'\n'\
'other page faults:         %R'

# language
export LANG=en_US.UTF-8

# editor
export EDITOR='vim'

# aliases

alias df="df -h"
alias ds="ds -h"

alias la="ls -la"

# include other files
if [ -d ~/.zsh-local ]; then
    for file in ~/.zsh-local/*.zsh; do
        source "$file"
    done
fi

