# sideshow.bob's .bashrc

# if not running interactively, don't do anything
[ -z "$PS1" ] && return

# source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

# load all custom definitions
for i in ~/.bash/*; do
    if [ -r "$i" ]; then
        if [ "$PS1" ]; then
            . "$i"
        else
            . "$i" > /dev/null
        fi
    fi
done

# colorized prompt
PS1="[\\[$(tput bold; tput setaf 2)\\]\\u@\\h:\\[$(tput sgr0; tput setaf 4)\\]\\w\\[$(tput sgr0)\\]] "

